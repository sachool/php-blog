<?php
/**
 * Blog Application
 *
 * @package ブログの設定
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/auth.php';

/******************
 *バリデーション
 * 入力値のチェック
 ******************/
$errors = array();//エラー初期化
//入力した値
$input_SITE_TITLE         = filter_input(INPUT_POST, '_SITE_TITLE');
$input_SITE_PHRASE        = filter_input(INPUT_POST, '_SITE_PHRASE');
$input_SITE_BLOG_PER_NUM  = filter_input(INPUT_POST, '_SITE_BLOG_PER_NUM');
$input_SITE_BLOG_BODY_ALL = filter_input(INPUT_POST, '_SITE_BLOG_BODY_ALL');
$input_SITE_BLOG_PER_NUM  = filter_input(INPUT_POST, '_SITE_BLOG_PER_NUM');


//POSTされた場合
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  //タイトル
  if(!isset($input_SITE_TITLE)||strlen($input_SITE_TITLE)<=0){
      $errors['_SITE_TITLE'] = '「タイトル」が入力されていません';
  }
  elseif(strlen($input_SITE_TITLE)>=50){
      $errors['_SITE_TITLE'] = '「タイトル」は50文字以内で入力してください';
  }
  //キャッチフレーズ
  if(!isset($input_SITE_PHRASE)||strlen($input_SITE_PHRASE)<=0){
      $errors['_SITE_PHRASE'] = '「キャッチフレーズ」が入力されていません';
  }
  elseif(strlen($input_SITE_PHRASE)>=100){
      $errors['_SITE_PHRASE'] = '「キャッチフレーズ」は100文字以内で入力してください';
  }
  //1ページに表示させる件数
  if(!isset($input_SITE_BLOG_PER_NUM)||strlen($input_SITE_BLOG_PER_NUM)<=0){
      $errors['_SITE_BLOG_PER_NUM'] = '「1ページに表示させる件数」が選択されていません';
  }
  //一覧に全文を表示させる
  if(!isset($input_SITE_BLOG_BODY_ALL)||strlen($input_SITE_BLOG_BODY_ALL)<=0){
      $errors['_SITE_BLOG_BODY_ALL'] = '「一覧に全文を表示させる」が選択されていません';
  }
  //ブログのデザイン
  if(!isset($input_SITE_BLOG_PER_NUM)||strlen($input_SITE_BLOG_PER_NUM)<=0){
      $errors['_SITE_BLOG_PER_NUM'] = '「ブログのデザイン」が選択されていません';
  }
  /******************
   *UPDATE処理
   ******************/
  if(empty($errors)){
      $post_data = $_POST;
      if(isset($post_data) && is_array($post_data)){
          foreach ($post_data as $key => $value) {
            if($key!="id" && $key!="token"){
                // ここでSQL文を作成します（パラメータはありません）
                $stmt = $mysqli->prepare("UPDATE settings SET blog_value=? WHERE blog_key=?");
                // ここでパラメータに実際の値となる変数を入れます。

                // sssdのところは、それぞれパラメータの型（string, string, string, double）を指定しています。
                $rc = $stmt->bind_param('ss', $value, $key);

                if ( false===$rc ) {
                    // again execute() is useless if you can't bind the parameters. Bail out somehow.
                    die('bind_param() failed: ' . htmlspecialchars($stmt->error));
                }
                /* プリペアドステートメントを実行します */
                $rc = $stmt->execute();
                if ( false===$rc ) {
                    die('execute() failed: ' . htmlspecialchars($stmt->error));
                }
                // $stmt->affected_rowsでクエリ結果を取得しています。これはInsert文などで変更された行の数を返します。
                //printf("%d Row inserted.\n", $stmt->affected_rows);
            }
              /* ステートメントと接続を閉じます */
              //
          }
          $stmt->close();//リソースの開放
      }
      //sessionの上書き
      $_SESSION['blog_login']['name'] = $input_name;
      header( "Location: /admin/setting/edit.php?flash_msg=setting updated." );
      exit(1);
  }
}
/****************
 * アカウントの表示
 * SELECT
 *****************/
 // プリペアステートメント
 $sql = "SELECT * FROM settings";

 if( $result = $mysqli->query( $sql ) ) {
     //echo 'SELECT成功';
     while( $row = $result->fetch_assoc() ) {
         /*
         echo '==========================================================';
         echo "id >>>> " . $row[ 'id' ] . "";
         echo "user_id >>> " . $row[ 'user_id' ] . "";
         echo "title >> " . $row[ 'title' ] . "";
         echo "body >> " . $row[ 'body' ] . "";
         echo "created >> " . $row[ 'created' ] . "";
         echo '==========================================================';
         */
         $settings[] = $row;
     }
     $result->close();
 }
 else {
     //echo 'SELECT失敗';
 }
 /*
if($_SESSION['blog_login']['id']){
    // プリペアステートメント
    $sql = "SELECT id, title, body FROM posts WHERE id=?";

    if ($stmt = $mysqli->prepare($sql)) {
        // 条件値をSQLにバインドする（補足参照）
        $stmt->bind_param("i", $post_id);
        // 実行
        $stmt->execute();
        $stmt->bind_result($user_id, $title, $body);
        while ($stmt->fetch()) {
            $s_id = $user_id;
            $s_title = $title;
            $s_body = $body;
        }

        if ($s_id) {
          //$row = $result->fetch_assoc();
          if($s_id){
            $smarty->assign("title", $s_title);
            $smarty->assign("body", $s_body);
            $smarty->assign("id", $s_id);
          }
          else{
            header( "Location: /404.php" ) ;
            exit(1);
          }
        }
        $stmt->close();
    }
}
else{
  header( "Location: /404.php" );
  exit(1);
}
*/

foreach ($settings as $key => $value) {
    $smarty->assign($value['blog_key'], $value['blog_value']);
}

$show_radios = array("1"=>"表示する","0"=>"表示しない");
$blog_design = array("1"=>"simple","2"=>"main-visual");
$blog_show_num = array("5"=>"5件","10"=>"10件");

$smarty->assign("errors", $errors);
$smarty->assign("show_radios", $show_radios);
$smarty->assign("blog_design", $blog_design);
$smarty->assign("blog_show_num", $blog_show_num);

$smarty->display('admin/setting/edit.html');
