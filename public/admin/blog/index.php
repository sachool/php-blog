<?php
/**
 * Blog Application
 *
 * @package Example-application
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/auth.php';
//echo "ようこそ".$_SESSION['blog_login']['name']."さん！";

//BLOGの一覧を取得する
// SQLの実行
//$sql = "SELECT * FROM posts WHERE user_id = '".$_SESSION['blog_login']['id']."' ORDER BY modified DESC";
$sql = "SELECT * FROM posts WHERE deleted IS NULL AND user_id = '".$_SESSION['blog_login']['id']."' ORDER BY modified DESC";

if( $result = $mysqli->query( $sql ) ) {
    //echo 'SELECT成功';
    while( $row = $result->fetch_assoc() ) {
        /*
        echo '==========================================================';
        echo "id >>>> " . $row[ 'id' ] . "";
        echo "user_id >>> " . $row[ 'user_id' ] . "";
        echo "title >> " . $row[ 'title' ] . "";
        echo "body >> " . $row[ 'body' ] . "";
        echo "created >> " . $row[ 'created' ] . "";
        echo '==========================================================';
        */
        $blogs[] = $row;
    }
    $result->close();
}
else {
    //echo 'SELECT失敗';
}

// Pager 準備
$perPage = _SITE_BLOG_PER_NUM;
$params = array(
    'mode'       => 'Jumping',
    'itemData'   => $blogs,
    'perPage'    => $perPage,
    //'delta'      => 10,
    //'clearIfVoid' => true,
    'prevImg' => '«',
    'nextImg' => '»',
    //'prevImg' => "前の{$perPage}件",
    //'nextImg' => "次の{$perPage}件",
    //'separator' => '|',
    //'expanded' => true,
    'curPageSpanPre' => '<li class="active"><a>',
    'curPageSpanPost' => '</a></li>'
);
$pager = Pager::factory($params);
$links = $pager->getLinks();
$data  = $pager->getPageData();
if ($pager->links) {
    //echo "<div>{$pager->links}</div>";
}

//print_r($blogs);
//print_r($links);

//templateへ変数を渡す
$smarty->assign("blogs", $data);
$smarty->assign("links", $links['all']);

$smarty->display('admin/blog/index.html');
