<?php
/**
 * Blog Application
 *
 * @package Blogの新規作成
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/auth.php';
//echo "ようこそ".$_SESSION['blog_login']['name']."さん！";
//templateの指定

/******************
 *バリデーション
 * 入力値のチェック
 ******************/
$errors = array();//エラー初期化
//入力した値
$input_title = filter_input(INPUT_POST, 'title');
$input_body = filter_input(INPUT_POST, 'body');

//POSTされた場合
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  //タイトル
  if(!isset($input_title)||strlen($input_title)<=0){
      $errors['title'] = 'タイトルが入力されていません';
  }
  elseif(strlen($input_title)>=100){
      $errors['title'] = 'タイトルは100文字以内で入力してください';
  }
  //本文
  if (!isset($input_body)||strlen($input_body)<=0) {
      $errors['body'] = '本文が入力されていません';
  }
} else{
    $auth = new Auth();
    //echo $auth->setToken();
}


/******************
 *登録処理
 ******************/
if($input_title && $input_body && !isset($errors['title']) && !isset($errors['body'])){
  // ここでSQL文を作成します（パラメータはありません）
	$stmt = $mysqli->prepare("INSERT INTO posts (user_id,title,body,created) VALUES (?, ?, ?, ?)");
	// ここでパラメータに実際の値となる変数を入れます。
	// sssdのところは、それぞれパラメータの型（string, string, string, double）を指定しています。
	$rc = $stmt->bind_param('isss', $_SESSION['blog_login']['id'], $input_title, $input_body, date('Y-m-d H:i:s'));

  if ( false===$rc ) {
    // again execute() is useless if you can't bind the parameters. Bail out somehow.
    die('bind_param() failed: ' . htmlspecialchars($stmt->error));
  }
	/* プリペアドステートメントを実行します */
  $rc = $stmt->execute();
  if ( false===$rc ) {
    die('execute() failed: ' . htmlspecialchars($stmt->error));
  }
	// $stmt->affected_rowsでクエリ結果を取得しています。これはInsert文などで変更された行の数を返します。
	//printf("%d Row inserted.\n", $stmt->affected_rows);

	/* ステートメントと接続を閉じます */
	$stmt->close();
  //INDEXへリダイレクト
  header( "Location: /admin/blog/?flash_msg=blog created." ) ;
  exit(1);
}

//templateへ変数を渡す
$smarty->assign("blog_login_name", $_SESSION['blog_login']['name']);
$smarty->assign("errors", $errors);
//入力内容の保持
$smarty->assign("title", $input_title);
$smarty->assign("body", $input_body);

$smarty->display('admin/blog/create.html');
