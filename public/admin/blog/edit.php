<?php
/**
 * Blog Application
 *
 * @package Blogの編集
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/auth.php';

//GETでブログの記事IDを取得
$post_id = filter_input(INPUT_GET, 'post_id');

/******************
 *バリデーション
 * 入力値のチェック
 ******************/
$errors = array();//エラー初期化
//入力した値
$input_title = filter_input(INPUT_POST, 'title');
$input_body = filter_input(INPUT_POST, 'body');
$input_id = filter_input(INPUT_POST, 'id');

//POSTされた場合
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  //タイトル
  if(!isset($input_title)||strlen($input_title)<=0){
      $errors['title'] = 'タイトルが入力されていません';
  }
  elseif(strlen($input_title)>=100){
      $errors['title'] = 'タイトルは100文字以内で入力してください';
  }
  //本文
  if (!isset($input_body)||strlen($input_body)<=0) {
      $errors['body'] = '本文が入力されていません';
  }
}


/******************
 *UPDATE処理
 ******************/
if($input_title && $input_body && !isset($errors['title']) && !isset($errors['body'])){
  // ここでSQL文を作成します（パラメータはありません）
	$stmt = $mysqli->prepare("UPDATE posts SET title=?, body=? WHERE id=?");
	// ここでパラメータに実際の値となる変数を入れます。
	// sssdのところは、それぞれパラメータの型（string, string, string, double）を指定しています。
	$rc = $stmt->bind_param('ssi', $input_title, $input_body, $input_id);

  if ( false===$rc ) {
    // again execute() is useless if you can't bind the parameters. Bail out somehow.
    die('bind_param() failed: ' . htmlspecialchars($stmt->error));
  }
	/* プリペアドステートメントを実行します */
  $rc = $stmt->execute();
  if ( false===$rc ) {
    die('execute() failed: ' . htmlspecialchars($stmt->error));
  }
	// $stmt->affected_rowsでクエリ結果を取得しています。これはInsert文などで変更された行の数を返します。
	//printf("%d Row inserted.\n", $stmt->affected_rows);

	/* ステートメントと接続を閉じます */
	$stmt->close();
  //INDEXへリダイレクト
  header( "Location: /admin/blog/?flash_msg=blog edited." ) ;
  exit(1);
}

/****************
 * Blog編集データの表示
 * SELECT
 *****************/
if($post_id){
  // プリペアステートメント
  $sql = "SELECT id, title, body FROM posts WHERE id=?";

  if ($stmt = $mysqli->prepare($sql)) {
      // 条件値をSQLにバインドする（補足参照）
      $stmt->bind_param("i", $post_id);
      // 実行
      $stmt->execute();
      $stmt->bind_result($user_id, $title, $body);
      while ($stmt->fetch()) {
          $s_id = $user_id;
          $s_title = $title;
          $s_body = $body;
      }

      if ($s_id) {
        //$row = $result->fetch_assoc();
        if($s_id){
          $smarty->assign("title", $s_title);
          $smarty->assign("body", $s_body);
          $smarty->assign("id", $s_id);
        }
        else{
          header( "Location: /404.php" ) ;
          exit(1);
        }
      }
      $stmt->close();
  }
}
else{
  header( "Location: /404.php" );
  exit(1);
}

$smarty->display('admin/blog/edit.html');
