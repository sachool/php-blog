<?php
/**
 * Blog Application
 *
 * @package Blogの削除
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/auth.php';
//GETでブログの記事IDを取得
$post_id = filter_input(INPUT_GET, 'post_id');
$confirm = filter_input(INPUT_GET, 'confirm');

//GETされた場合
if ($_SERVER['REQUEST_METHOD'] == 'GET' && $post_id && $confirm == TRUE){
  $sql = "SELECT id, title, body FROM posts WHERE id=?";
  if ($stmt = $mysqli->prepare($sql)) {
      // 条件値をSQLにバインドする（補足参照）
      $stmt->bind_param("i", $post_id);
      // 実行
      $stmt->execute();
      $result = $stmt->get_result();

      if ($result->num_rows >= 1) {
        // ここでSQL文を作成します（パラメータはありません）
      	$stmt = $mysqli->prepare("UPDATE posts SET deleted=? WHERE id=?");
      	// ここでパラメータに実際の値となる変数を入れます。
      	// sssdのところは、それぞれパラメータの型（string, string, string, double）を指定しています。
      	$rc = $stmt->bind_param('si', date('Y-m-d H:i:s'), $post_id);

        if ( false===$rc ) {
          // again execute() is useless if you can't bind the parameters. Bail out somehow.
          die('bind_param() failed: ' . htmlspecialchars($stmt->error));
        }
      	/* プリペアドステートメントを実行します */
        $rc = $stmt->execute();
        if ( false===$rc ) {
          die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
      	// $stmt->affected_rowsでクエリ結果を取得しています。これはInsert文などで変更された行の数を返します。
      	//printf("%d Row inserted.\n", $stmt->affected_rows);

      	/* ステートメントと接続を閉じます */
      	$stmt->close();
        //INDEXへリダイレクト
        header( "Location: /admin/blog/?flash_msg=blog deleted." ) ;
        exit(1);
      }
  }
}

///INDEXへリダイレクト
header( "Location: /admin/blog/" ) ;
exit(1);
