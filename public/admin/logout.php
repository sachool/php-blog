<?php
//セッション開始
session_start();
//sessionの破棄
session_destroy();
unset($_SESSION['blog_login']);

header( "Location: /login.php" ) ;
exit(1);

?>
