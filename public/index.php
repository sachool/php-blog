<?php
/**
 * Blog Application
 *
 * @package ブログトップ
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';
//GETでブログのページを取得
$post_id = filter_input(INPUT_GET, 'post_id');


//BLOGの一覧を取得する
$sql = "SELECT * FROM posts WHERE deleted IS NULL ORDER BY modified DESC";
if( $result = $mysqli->query( $sql ) ) {
    while( $row = $result->fetch_assoc() ) {
        $blogs[] = $row;
    }
    $result->close();
}
else {
}

// Pager 準備
$params = array(
    'mode'       => 'Jumping',
    'itemData'   => $blogs,
    'perPage'    => $_BLOG_SETTING['_SITE_BLOG_PER_NUM'],
    //'delta'      => 10,
    //'clearIfVoid' => true,
    'prevImg' => '← newer',
    'nextImg' => 'older →',
    //'prevImg' => "前の{$perPage}件",
    //'nextImg' => "次の{$perPage}件",
    //'separator' => '|',
    //'expanded' => true,
    'curPageSpanPre' => '<li class="active"><a>',
    'curPageSpanPost' => '</a></li>'
);
$pager = Pager::factory($params);
$links = $pager->getLinks();
$data  = $pager->getPageData();

//templateへ変数を渡す
$smarty->assign("blogs", $data);
$smarty->assign("links", $links);
$smarty->assign("linkss", $links['all']);


//print_r($data);
switch ($_BLOG_SETTING['_SITE_BLOG_DESIGN']) {
  case '1':
    $smarty->display('index.html');
    break;
  case '2':
    $smarty->display('index2.html');
    break;
  default:
      $smarty->display('index.html');
    break;
}
