<?php
/**
 * Blog Application
 *
 * @package 404 page
 */

require '../app/libs/Smarty.class.php';

$smarty = new Smarty;
$smarty->display('404.html');
