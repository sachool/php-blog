<?php
/**
 * Example Application
 *
 * @package Example-application
 */
require str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'app/config/config.php';

/******************
 *バリデーション
 * 入力値のチェック
 ******************/
$errors = array();//エラー初期化
//入力した値
$input_email = filter_input(INPUT_POST, 'email');
$input_password = filter_input(INPUT_POST, 'password');

//POSTされた場合
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

  //Eメールアドレス
  if(!isset($input_email)){
      $errors['email'] = 'Eメールアドレスが入力されていません';
  }
  elseif(strlen($input_email)>=100){
      $errors['email'] = 'Eメールアドレスは100文字以内で入力してください';
  }
  else{
      if (filter_var($input_email, FILTER_VALIDATE_EMAIL) !== false) {
        //echo '正しいEメールアドレス形式です';
      }
      else{
        $errors['email'] = 'Eメールアドレスの形式が不正です';
      }
  }

  //パスワード
  if (!isset($input_password)) {
      $errors['password'] = 'パスワードが送信されていません';
  } elseif (strlen($input_password)>15 || strlen($input_password)<=6) {
      $errors['password'] = '6文字以上15文字未満で入力してください';
  } else {
      if (preg_match("/^[a-zA-Z0-9]+$/", $input_password)) {
        //echo '正しいパスワード形式です';
      }
      else{
        $errors['password'] = 'パスワードの形式が不正です';
      }
  }
}
/******************
 *ログイン処理
 ******************/
if($input_email && $input_password && !isset($errors['email']) && !isset($errors['password'])){
	// SELECT文のひな型をもとにステートメントハンドルを取得する
  // プリペアステートメント
	$sql = "SELECT id, name FROM users WHERE email=? AND password=?";
	if ($stmt = $mysqli->prepare($sql)) {
	    // 条件値をSQLにバインドする（補足参照）
	    $stmt->bind_param("ss", $input_email, $input_password);
	    // 実行
	    $stmt->execute();
	    // 取得結果を変数にバインドする
	    $stmt->bind_result($id, $name);

      //ログイン成功
      while ($stmt->fetch()) {
        if($id && $name){
          //echo "ID=$id, NAME=$name<br>";
          $_SESSION['blog_login']['id'] = $id;
          $_SESSION['blog_login']['name'] = $name;
          header( "Location: /admin/" ) ;
          exit(1);
        }
      }
      //ログイン失敗
      $errors['login'] = 'メールアドレスまたはパスワードが違います。';
      echo $_SESSION['login_id'];

	    $stmt->close();
	}
}


//$smarty->force_compile = true;
//$smarty->debugging = false;
//$smarty->caching = false;
//$smarty->cache_lifetime = 120;

//templateへエラーを渡す
$smarty->assign("errors", $errors);
//templateの指定
$smarty->display('login.html');
