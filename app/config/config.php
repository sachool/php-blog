<?php
/*
 * 初期設定ファイル
 * config.php
 */
//セッション開始
session_start();

//環境定数の読み込み
require 'env.php';
//smartyの読み込み
require LIBS_DIR.'Smarty.class.php';
//databaseの読み込み
require CONFIG_DIR.'database.php';
//vendor(composer)のライブラリ読み込み
require PROJECT_DIR.'vendor/autoload.php';

//smartyをnew
$smarty = new Smarty;

//flash Message
$flash_msg = (filter_input(INPUT_GET, 'flash_msg')) ? filter_input(INPUT_GET, 'flash_msg') : '';
$smarty->assign("flash_msg", $flash_msg);

//ブログの設定をDBから取得
// プリペアステートメント
$sql = "SELECT * FROM settings";
$mysqli->set_charset("utf8");
if( $result = $mysqli->query( $sql ) ) {
    //echo 'SELECT成功';
    while( $row = $result->fetch_assoc() ) {
        $_BLOG_SETTINGS[] = $row;
    }
    foreach ($_BLOG_SETTINGS as $key => $value) {
        $_BLOG_SETTING[$value['blog_key']] = $value['blog_value'];
        $smarty->assign($value['blog_key'], $value['blog_value']);
    }
    $result->close();
}
else {
    //echo 'SELECT失敗';
}

?>
