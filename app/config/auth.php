<?php
/*
 * 管理側用
 * auth.php
 */
class Auth{

  public function __construct()
  {
    # code...
  }

  public function login_check(){
    if(!isset($_SESSION['blog_login']['id']) && !isset($_SESSION['blog_login']['name'])){
      header( "Location: /login.php?flash_msg=再度ログインしてください." ) ;
      exit(1);
    }
  }

  //トークンをセッションにセット
  public function setToken(){
    $token = sha1(uniqid(mt_rand(), true));
    $_SESSION['token'] = $token;
    return $token;
  }

  //トークンをセッションから取得
  public function checkToken(){
      print_r($_SESSION);
      echo $_POST['token'];
      //セッションが空か生成したトークンと異なるトークンでPOSTされたときは不正アクセス
      if(empty($_SESSION['token']) || ($_SESSION['token'] != $_POST['token'])){
        echo '不正なPOSTが行われました';
        exit;
      }
  }
}
/*
 * loginチェック
 */
$auth = new Auth();
$auth->login_check();

/*
 * CSRF対策
 */
//GETでアクセスされたとき
if($_SERVER['REQUEST_METHOD'] != 'POST'){
    $token = $auth->setToken();
    $smarty->assign("token", $token);
}
//POSTでアクセスされたとき
else{
    //$auth->checkToken();
}


?>
