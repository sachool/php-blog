<?php
/*************************************************************************
 * データベース接続
 * これは "公式な" オブジェクト指向のやりかたですが、
 * PHP 5.2.9 および 5.3.0 より前のバージョンでは $connect_error は動作していませんでした
 *************************************************************************/
$mysqli = new mysqli('localhost', 'blog_user', 'blog_password', 'blog_db');

if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '. $mysqli->connect_error);
} else {
	//echo 'DB接続しました。';
    $mysqli->set_charset("utf8");
}
?>
