<?php
/*
 * 環境変数用
 * env.php
 */
define('PROJECT_DIR','/vagrant/blog-project/');
define('APP_DIR',PROJECT_DIR.'app/');
define('LIBS_DIR',APP_DIR.'libs/');
define('TEMPLATE_DIR',APP_DIR.'templates/');
define('CONFIG_DIR',APP_DIR.'config/');
define('PUB_DIR',PROJECT_DIR.'public/');

/*
 * サイト設定
 */
define('_SITE_TITLE','ブログタイトル');//ブログのタイトル
define('_SITE_PHRASE','ブログキャッチフレーズ');//ブログのキャッチフレーズ
define('_SITE_BLOG_PER_NUM','5');//1ページに表示させる件数
define('_SITE_BLOG_BODY_ALL','0');//一覧に全文を表示させる:1、表示させない:0
define('_SITE_BLOG_DESIGN','2');//simple:1、main-visual:2
// mariadb
//INSERT INTO settings VALUES ('_SITE_TITLE','ブログタイトル',now(),now());
//INSERT INTO settings VALUES ('_SITE_PHRASE','ブログキャッチフレーズ',now(),now());
//INSERT INTO settings VALUES ('_SITE_BLOG_PER_NUM','5',now(),now());
//INSERT INTO settings VALUES ('_SITE_BLOG_BODY_ALL','0',now(),now());
//INSERT INTO settings VALUES ('_SITE_BLOG_DESIGN','2',now(),now());
